<?php

namespace PhpMonitoring\Modules\Cas;

use PhpMonitoring\Modules\AbstractModules;

abstract class AbstractCas extends AbstractModules {

  protected $msg = [
    'noconn' => 'could not connect',
    'noconnfull' => 'could not connect at url: '
  ];

  abstract protected function checkConnection();

  public function doTheMagic() {

    $ok = $this->checkConnection();

    if (!$ok)
      return;
   
    $this->status = 'ok';
  }

}
