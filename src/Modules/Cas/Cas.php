<?php

namespace PhpMonitoring\Modules\Cas;

Class Cas extends AbstractCas {

  protected function checkConnection() {

    $url = $this->config['url'] . '/login';

    $header = get_headers($url);

    if (empty($header)) {
      $this->status = $this->getMessage('noconn');
      $this->errorMessage = $this->getMessage('noconnfull') . $url;
      return false;
    } else {
      return true;
    }
  }

}
