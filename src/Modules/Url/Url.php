<?php

namespace PhpMonitoring\Modules\Url;

Class Url extends AbstractUrl {

  protected function checkStatus(&$udata) {

    $http_code = $this->response['response_info']['http_code'];

    $expected = isset($udata['http_code']) ? (int) $udata['http_code'] : 200;

    if ($http_code !== $expected) {
      $this->status = $this->getMessage('statuscode');
      $this->errorMessage = $this->getMessage('statuscodefull') . $http_code;
      throw new \Exception('fail');
    }
  }

  protected function checkConnection(&$udata) {

    if (empty($this->response['response_header'])) {

      $this->status = $this->getMessage('noconn');
      $this->errorMessage = $this->getMessage('noconnfull') . $udata['uri'];
      throw new \Exception('fail');
    }
  }

}
