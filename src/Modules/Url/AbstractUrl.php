<?php

namespace PhpMonitoring\Modules\Url;

use PhpMonitoring\Modules\AbstractModules;

abstract class AbstractUrl extends AbstractModules {

  protected $msg = [
      'noconn' => 'could not connect',
      'noconnfull' => 'could not connect at url: ',
      'statuscode' => 'status code not expected',
      'statuscodefull' => 'Was expected a status code 200, but return: ',
  ];
  
  protected $url = [];
  
  abstract protected function checkStatus(&$udata);

  abstract protected function checkConnection(&$udata);

  public function doTheMagic() {

    try {

      foreach ($this->config['url'] as &$udata) {

        $this->getUrl($udata);
        $this->checkConnection($udata);
        $this->checkStatus($udata);
      }

      $this->status = 'ok';
      return true;
    } catch (\Exception $e) {
      return false;
    }
  }

  protected function getUrl(&$udata) {

    $uri = $udata['uri'];

    $method = strtoupper($udata['http']['method']);
    if (empty($method))
      $method = 'GET';

    $cu = curl_init();
    curl_setopt($cu, CURLOPT_RETURNTRANSFER, TRUE);

    switch ($method) {

      case 'DELETE':

        curl_setopt($cu, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($cu, CURLOPT_POSTFIELDS, http_build_query($udata['http']['data']));

        break;

      case 'POST':

        curl_setopt($cu, CURLOPT_POST, TRUE);
        curl_setopt($cu, CURLOPT_POSTFIELDS, $udata['http']['data']);

        break;

      case 'PUT':

        curl_setopt($cu, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($cu, CURLOPT_POSTFIELDS, $udata['http']['data']);

        break;

      case 'GET':

        $uri .= '?' . http_build_query($udata['http']['data']);

        break;
      default :

        throw new Exception('Method not Allowed:' . $method);
    }

    curl_setopt($cu, CURLOPT_HTTPHEADER, $this->makeHeader($udata['http']['header']));
    curl_setopt($cu, CURLOPT_URL, $uri);
    curl_setopt($cu, CURLOPT_HEADER, true);
    curl_setopt($cu, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($cu, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($cu, CURLINFO_HEADER_OUT, true);


    $response = curl_exec($cu);
    $response_info = curl_getinfo($cu);
    $request_sent = curl_getinfo($cu, CURLINFO_HEADER_OUT);

    curl_close($cu);

    $this->response = [
        response_header => trim(substr($response, 0, $response_info['header_size'])),
        response_body => substr($response, $response_info['header_size']),
        response_info => $response_info,
        request_sent => $request_sent
    ];
  }

  private function makeHeader($header) {

    $arrHeader = [];

    foreach ($header as $name => $val) {
      $arrHeader[] = "{$name}: {$val}";
    }

    return $arrHeader;
  }

}
