<?php

namespace PhpMonitoring\Modules;

Abstract Class AbstractModules implements iModules {

  protected $status = '';
  protected $errorMessage = '';
  protected $config;
  protected $msg = [];
  protected static $environments;

  public function setConfig(&$config) {
    $this->config = $config;
  }

  public function &getStatus() {
    return $this->status;
  }

  static function setEnvironments(&$environments) {
    self::$environments = &$environments;
  }

  public function getMessage($key) {
    return $this->msg[$key];
  }
  
  public function getErrorMessage(){
    return $this->errorMessage;
  }
  

}
