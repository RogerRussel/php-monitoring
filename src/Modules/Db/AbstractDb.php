<?php

namespace PhpMonitoring\Modules\Db;

use PhpMonitoring\Modules\AbstractModules;

abstract class AbstractDb extends AbstractModules {

  protected $db;

  protected $msg = [
    'noconnect' => 'was not possible connect into SGDB',
    'noresult' => 'don\'t had and result',
  ];

  abstract protected function testQueryReturn();

  public function doTheMagic() {

    if (!$this->connectDb()) {
      $this->status = $this->getMessage('noconnect');
    } else {
      $this->testQueryReturn();
    }
  }

  protected function connectDb() {

    $conn = $this->config['conn'] ? $this->config['conn'] : 'Default';

    $this->db = &self::$environments['db'][$conn]['connection'];

    if (get_class($this->db) !== 'PDO')
      return false;

    return true;
  }

}
