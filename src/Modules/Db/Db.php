<?php

namespace PhpMonitoring\Modules\Db;

use \PDO;

Class Db extends AbstractDb {

  protected function testQueryReturn() {

    $ok = true;

    foreach ($this->config['querys'] as $i => $sql) {

      $sth = $this->db->prepare($sql);
      $sth->execute();
      $result = $sth->fetchAll(PDO::FETCH_ASSOC);

      if (count($result) < 1) {
        $ok = false;
        break;
      }
    }

    if ($ok) {
      $this->status = 'ok';
    } else {
      $this->status = "query: $sql; {$this->msg['noresult']}";
    }
  }

}
