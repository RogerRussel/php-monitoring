<?php

namespace PhpMonitoring\Modules\Nfs;

Class Nfs extends AbstractNfs {
   
  protected function checkIfPathIsMounted() {

   $cmd = 'stat -fc%t:%T ';
    
   $pathFather = realpath($this->path . '/../');
   
   $p = shell_exec($cmd . $this->path);
   $f = shell_exec($cmd . $pathFather);
   
   return $p === $f ? false: true;
  }
}