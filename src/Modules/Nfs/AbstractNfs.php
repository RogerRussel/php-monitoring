<?php

namespace PhpMonitoring\Modules\Nfs;

use PhpMonitoring\Modules\AbstractModules;

abstract class AbstractNfs extends AbstractModules {

  protected $path;
  protected $msg = [
    'noexist' => 'path doesn\'t exist',
    'nonmounted' => 'is not mounted'
  ];

  abstract protected function checkIfPathIsMounted();

  public function doTheMagic() {

    $this->resolvePath();

    if (empty($this->path)) {
      $this->status = $this->getMessage('noexist');
      return;
    }

    $ok = $this->checkIfPathIsMounted();
    $this->status = $ok ? 'ok' : $this->getMessage('nonmounted');
  }

  protected function resolvePath() {

    $rootDir = '';
    
    if (isset(self::$environments['rootDir'])){
       $rootDir = self::$environments['rootDir'];
    }
    
    $this->path = realpath(str_replace('[rootDir]', $rootDir, $this->config['path']));
  }

}
