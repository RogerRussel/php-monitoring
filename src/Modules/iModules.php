<?php

namespace PhpMonitoring\Modules;

interface iModules {

  function setConfig(&$config);

  function &getStatus();

  function getMessage($key);

  static function setEnvironments(&$environments);

  function doTheMagic();
}
