<?php

namespace PhpMonitoring;

use \PhpMonitoring\Modules\AbstractModules;

Class Monitor implements iMonitor {

  protected $config = [];
  protected $data = [];
  protected $rootDir = [];
  protected $environments = [];

  /**
   * 
   * @param type $rootDir
   */
  public function setRootDir($rootDir) {
    $this->environments['rootDir'] = $rootDir;
  }

  /**
   * Path to config file
   * @param string $configPath
   */
  public function readConfigFile($configPath) {
    $this->config = require $configPath;
  }

  /**
   * 
   * @return type
   */
  public function &getJson() {
    header('Content-Type: application/json');
    return json_encode($this->data);
  }

  /**
   * 
   * @param type $class
   * @return type
   */
  protected function newModule($class) {

    $class_with_name_space = "PhpMonitoring\\Modules\\{$class}\\{$class}";

    return new ${'class_with_name_space'};
  }

  /**
   * 
   * @param type $con
   * @param type $conName
   */
  public function linkDbConnect(&$con, $conName = 'Default') {

    if (!isset($this->environments['db'])) {
      $this->environments['db'] = [];
    }

    $this->environments['db'][$conName] = [
      'connection' => & $con,
    ];
  }

  /**
   * 
   */
  public function doTheMagic() {

    AbstractModules::setEnvironments($this->environments);

    foreach ($this->config as $mconf) {
      $module = $this->newModule($mconf['type']);
      $config = &$mconf['config'];
      $module->setConfig($config);
      $module->doTheMagic();

      $data = [
        'name' => $config['name'],
        'status' => $module->getStatus()
      ];

      if ($data['status'] !== 'ok') {

        $data['error'] = $config['error'];

        $msg = $module->getErrorMessage();

        if (!empty($msg))
          $data['msg'] = $msg;
      }

      $this->data[] = $data;
    }
  }

}
