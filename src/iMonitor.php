<?php

namespace PhpMonitoring;

Interface iMonitor {
  
  function readConfigFile($configPath);
  function &getJson();
  function linkDbConnect(&$con);
  function doTheMagic();
  
}