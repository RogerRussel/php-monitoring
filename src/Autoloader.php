<?php

namespace PhpMonitoring;

Class Autoloader {

  static $loaded = [];
  
  public function __construct() {
    spl_autoload_register([$this, 'loader']);
  }

  /**
   * 
   * @param string $class
   * @return void
   */
  private function loader($class) {

    $match = [];

    if (!preg_match("/PhpMonitoring\\\\(.*?)$/", $class, $match))
      return;

    if(isset($this->loaded[$class]))
      return;
    
    require __DIR__ . '/' . str_replace('\\', '/', $match[1]) . '.php';
    
    $this->loaded[$class] = true;
    
  }

}

