<?php

spl_autoload_register(function($class) {
  $match = [];

  if (!preg_match("/Module\\\\(.*?)$/", $class, $match))
    return;

  require __DIR__ . '/Module/' . str_replace('\\', '/', $match[1]) . '.php';

});
