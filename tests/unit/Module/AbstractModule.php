<?php

require_once __DIR__ . '/iModule.inc';

Abstract class AbstractModule extends \Codeception\TestCase\Test implements iModule {

  use \Codeception\Specify;

  protected $subject;

  abstract protected function newSubject();
}
