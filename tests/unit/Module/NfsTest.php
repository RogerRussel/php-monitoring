<?php

require_once __DIR__ . '/AbstractModule.php';

class NfsTest extends AbstractModule {

  use \Codeception\Specify;

  /**
   * @var \UnitTester
   */
  protected $tester;
  protected $subject;

  protected function _before() {
    
  }

  protected function _after() {
    
  }

  protected function newSubject() {
    return new Module\Helper\Nfs();
  }

  public function testresolvePath() {

    $this->specify("resolvePath have to change rootdir", function() {

      try {

        $subject = $this->newSubject();

        $config = [
          'error' => 2,
          'name' => 'storage',
          'path' => '[rootdir]/unmounted',
          'rootdir' => __DIR__ . '/Fixtures',
        ];

        $subject->setConfig($config);
        $subject->resolvePath();

        $this->assertEquals($subject->getPath(), __DIR__ . '/Fixtures/unmounted');
      } catch (\Exception $e) {
        $this->assertTrue($e->getMessage());
      }
    });

    $this->specify("resolvePath to a non existent path should given a status error", function() {
      try {

        $subject = $this->newSubject();

        $config = [
          'error' => 2,
          'name' => 'storage',
          'path' => '[rootdir]/noexists',
          'rootdir' => __DIR__ . '/Fixtures',
        ];

        $subject->setConfig($config);
        $subject->doTheMagic();

        $status = $subject->getStatus();

        $this->assertNotEmpty($status);

        $this->assertEquals($status, $subject->getMessage('noexist'));
      } catch (\Exception $e) {
        $this->assertTrue($e->getMessage());
      }
    });
  }

  public function testcheckIfPathIsMounted() {

    $this->specify("verify if path is not mounted", function() {

      try {

        $subject = $this->newSubject();

        $config = [
          'error' => 2,
          'name' => 'storage',
          'path' => '[rootdir]/unmounted',
          'rootdir' => __DIR__ . '/Fixtures',
        ];

        $subject->setConfig($config);
        $subject->doTheMagic();

        $this->assertEquals($subject->getStatus(), $subject->getMessage('nonmounted'));
      } catch (\Exception $e) {
        $this->assertTrue($e->getMessage());
      }
    });

    $this->specify("verify if path is mounted", function() {

      try {

        $subject = $this->newSubject();

        $fixtureDir = __DIR__ . '/Fixtures';
        
        $cmd = "mount -o loop {$fixtureDir}/floppy-grub4dos {$fixtureDir}/mounted";
        
        shell_exec($cmd);
        
        $config = [
          'error' => 2,
          'name' => 'storage',
          'path' => $fixtureDir . '/mounted',
        ];

        $subject->setConfig($config);
        $subject->doTheMagic();

        $this->assertEquals($subject->getStatus(),'ok');
      } catch (\Exception $e) {
        $this->assertTrue($e->getMessage());
      }
    });
  }

}
